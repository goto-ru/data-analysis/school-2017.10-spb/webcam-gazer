from sklearn.ensemble import RandomForestClassifier
import numpy as np

import pandas as pd
df = pd.DataFrame([])


def dataset_shape(pointer_mouse, pointer_gaze, points, keys):
    arr = []
    for key in points:
        for point in points[key]:
            tmp = []
            point_to_arr = [0]*6
            if point == "left-top":
                point_to_arr[0] = 1
            if point == "left-bottom":
                point_to_arr[1] = 1
            if point == "right-top":
                point_to_arr[2] = 1
            if point == "right-bottom":
                point_to_arr[3] = 1
            if point == "center":
                point_to_arr[4] = 1
            if point == "header":
                point_to_arr[5] = 1
            tmp += pointer_gaze + points[key][point] + point_to_arr

            if key == keys[0] and point == keys[1] and pointer_mouse[2] == 1:
                tmp += [1]
            else:
                tmp += [0]

            arr += [tmp]
    return arr


def model_train():
    df = pd.DataFrame.from_csv("dataset.csv")
    x = np.array(df[df.columns[:10]])
    y = np.array(df[df.columns[10]])

    model = RandomForestClassifier(max_depth=10, random_state=0)
    model.fit(x,y)

    return model