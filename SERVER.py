from flask import Flask, render_template, request
import numpy as np

import window_position as wp
import classification as cs

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template('demo.html')


@app.route('/write', methods=['GET', 'POST'])
def writer():
    pred_coord = [int(x) for x in str(request.args['text']).split()]
    points = wp.get_all_points()

    pointer = wp.get_mouse_position()
    keys = wp.closest_point(pointer, points)

    dataset = cs.dataset_shape(pointer, pred_coord, points, keys)

    # cs.df = cs.df.append(dataset)
    # cs.df.to_csv("dataset.csv")

    probs = [model.predict_proba([x[:10]])[0][1] for x in dataset]
    # print(np.argmax(probs), np.max(probs))
    if np.max(probs) > 0.3:
        corner = dataset[np.argmax(probs)]
        print(corner[2:4])


    return ''


if __name__ == "__main__":
    model = cs.model_train()
    app.run(ssl_context='adhoc', debug=True)
