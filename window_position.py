import win32gui
import win32api


def enum_window_handles():
    def callback(handle, data):
        if win32gui.IsWindowVisible(handle) and not win32gui.IsIconic(handle):
            handles.append(handle)
    handles = []
    win32gui.EnumWindows(callback, None)
    return handles


def visible_points(handles):
    corners = {}
    for handle in handles:
        corners[handle] = {}
        rect = win32gui.GetWindowRect(handle)
        if win32gui.WindowFromPoint((rect[0], rect[1])) == handle:
            corners[handle]["left-top"] = [rect[0], rect[1]]
        if win32gui.WindowFromPoint((rect[0], rect[3] - 1)) == handle:
            corners[handle]["left-bottom"] = [rect[0], rect[3]]
        if win32gui.WindowFromPoint((rect[2] - 1, rect[1])) == handle:
            corners[handle]["right-top"] = [rect[2], rect[1]]
        if win32gui.WindowFromPoint((rect[2] - 1, rect[3] - 1)) == handle:
            corners[handle]["right-bottom"] = [rect[2], rect[3]]
        if win32gui.WindowFromPoint((int((rect[2]-rect[0])/2+rect[0]), int((rect[3]-rect[1])*0.5+rect[1]))) == handle:
            corners[handle]["center"] = [int((rect[2]-rect[0])/2+rect[0]), int((rect[3]-rect[1])*0.5+rect[1])]
        if win32gui.WindowFromPoint((int((rect[2]-rect[0])/2+rect[0]), int((rect[3]-rect[1])*0.02+rect[1]))) == handle:
            corners[handle]["header"] = [int((rect[2]-rect[0])/2+rect[0]), int((rect[3]-rect[1])*0.02+rect[1])]
    return corners


def get_all_points():
    points = visible_points(enum_window_handles())
    return points


def get_mouse_position():
    pointer = [x for x in win32api.GetCursorPos()]
    if win32api.GetKeyState(0x01) in (0, 1):
        state_left = 0
    else:
        state_left = 1
    pointer += [state_left]
    return pointer


def closest_point(pointer, points):
    sum = 1e10
    keys = []
    for key in points:
        for point in points[key]:
            x = points[key][point][0]
            y = points[key][point][1]
            if abs(pointer[0]-x)+abs(pointer[1]-y) < sum:
                sum = abs(pointer[0]-x)+abs(pointer[1]-y)
                keys = [key, point]
    return keys