# WebcamGazer
Современные EyeTracking системы представляют из себя либо сложное оборудование, либо крайне неточные данные. 
В данном проекте я попытался исправить неточность. Предполагается, что во время работы с компьютером человек хочет совершить какое-либо действие с окном, когда смотрит на активный элемент.
Для уточнения сырых данных я использовал это предположение, а именно - получал видимые углы окон и используя машинное обучение определял смотрит ли на них человек или нет.